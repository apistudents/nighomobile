// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var Nigho = angular.module('Nigho', ['ionic', 'pascalprecht.translate']);

Nigho.run(function($ionicPlatform, $translate) {
  $ionicPlatform.ready(function() {
    if (typeof navigator.globalization !== "undefined") {
            navigator.globalization.getPreferredLanguage(function (language) {
                $translate.use((language.value).split("-")[0]).then(function (data) {
                    alert(data);
                    // si on veut rajouter un message de succes
                }, function (error) {
                    // si on veut rajouter un message d'erreur
                });
            }, null);
        }
  });
});

// Constante de l'application
Nigho.constant("MY_CONSTANT", {
    "MAIN_FOLDER": ".Nigho",
});

Nigho.config(function ($stateProvider, $urlRouterProvider, $translateProvider, $sceDelegateProvider) {

    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        'http://report.nigho.com/**'
    ]);

    // gestion de i18n
    $translateProvider.translations("en", {
        starterTitle: "Nigho",
        //Login Page
        loginTitle: "Login",
        loginUsername: "Username",
        loginPassword: "Password",
        login: "Log in",
        loginCreateAccount: "Or create an account",
        loginForgotPassword: "Forgot password",
        //Register Page
        registerTitle: "Register",
        registerUsername: "Username",
        registerPassword: "Password",
        registerMail: "Email",
        registerSignUp: "Sign up",
        registerSignUpFacebook: "Sign up with Facebook",
        registerSignUpLinkedin: "Sign up with LinkedIn",
        registerSignUpGoogle: "Sign up with Google", 
        //Back Button
        backButton: "Back",
        //Forgot Password Page
        forgotTitle: "Forgot Password",
        forgotMail: "Email",
        forgotSend: "Send",
        forgotRegister:"Or create an account",
        //Footer
        footerHome: "Home",
        footerOptions: "Options",
        //Menu
        menuTitle: "Menu",
        menuProfil: "Profil",
        menuContact: "Contacts",
        menuSite: "Sites",
        menuReport: "Reports",
        //Error
        errorAuthentification: "Authentication error",
        errorInputEmpty: "Please fill in all fields",
        errorInputUsername: "Invalid username",
        errorInputPassword: "Invalid password",
        errorInputMail: "Invalid mail address",
        errorRegister: "Registration error",
        errorAuthFailed: "User already used",
        errorServer: "Server problem, try again later",
        errorConnectionReport: "Connect you to see the report",
        errorConnectionSites: "Connect you to refresh the sites list",
        //Utilitaire
        searchGlobal: "Search"
    });

    $translateProvider.translations("fr", {
        starterTitle: "Nigho",
        //Login Page
        loginTitle: "Connexion",
        loginUsername: "Pseudo",
        loginPassword: "Mot de passe",
        login: "Se connecter",
        loginCreateAccount: "S'inscrire",
        loginForgotPassword: "Mot de passe oublié ?",
        //Register Page
        registerTitle: "Inscription",
        registerUsername: "Pseudo",
        registerPassword: "Mot de passe",
        registerMail: "EMail",
        registerSignUp: "S'inscrire",
        registerSignUpFacebook: "S'inscrire via Facebook",
        registerSignUpLinkedin: "S'inscrire via Linkedin",
        registerSignUpGoogle: "S'inscrire via Google", 
        //Back Button
        backButton: "Retour",
        //Forgot Password Page
        forgotTitle: "Mot de passe oublié",
        forgotMail: "Email",
        forgotSend: "Envoyer",
        forgotRegister:"S'inscrire",
        //Footer
        footerHome: "Accueil",
        footerOptions: "Options",
        //Menu
        menuTitle: "Menu",
        menuProfil: "Profil",
        menuContact: "Contacts",
        menuSite: "Sites",
        menuReport: "Rapports",
        //Error
        errorAuthentification: "Erreur d'authentification",
        errorInputEmpty: "Veuillez remplir tous les champs",
        errorInputUsername: "Pseudo non valide",
        errorInputPassword: "Mot de passe non valide",
        errorInputMail: "Adresse mail non valide",
        errorRegister: "Erreur lors de l'inscription",
        errorAuthFailed: "Utilisateur déjà utilisé",
        errorServer: "Serveur en maintenance, réessayez plus tard",
        errorConnectionReport: "Connecte toi pour voir le rapport",
        errorConnectionSites: "Connecte toi pour rafraichir la liste des sites",
        //Utilitaire
        searchGlobal: "Recherche"
    });
    $translateProvider.preferredLanguage("en");
    $translateProvider.fallbackLanguage("en");

    // gestion de la naviguation dans l'app
    $stateProvider
            .state('login', {
                cache: true,
                url: "/login",
                templateUrl: "login"
            })

            .state('register', {
                url: "/register",
                cache: true,
                templateUrl: "register"
            })

            .state('forgottenPassword', {
                cache: true,
                url: "/forgottenPassword",
                templateUrl: "forgottenPassword"
            })

            .state('main', {
                cache: true,
                url: "/main",
                templateUrl: "main"
            })

            .state('sites', {
                cache: true,
                url: "/sites",
                templateUrl: "sites"
            })

            .state('contacts', {
                cache: true,
                url: "/contacts",
                templateUrl: "contacts"
            })

            .state('reports', {
                cache: true,
                url: "/reports",
                templateUrl: "reports"
            })

            .state('detailcontact', {
                cache: true,
                url: "/detailcontact",
                templateUrl: "detailcontact"
            })

            .state('addcontact', {
                cache: true,
                url: "/addcontact",
                templateUrl: "addcontact"
            })

            .state('updatecontact', {
                cache: true,
                url: "/updatecontact",
                templateUrl: "updatecontact"
            })

    $urlRouterProvider.otherwise('/login');
});

Nigho.config(['$ionicConfigProvider', function($ionicConfigProvider) {

    $ionicConfigProvider.tabs.position('bottom'); // other values: top

}]);

// directive for the naviguation in the application

Nigho.directive("fichierLogin", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/login.html"
    }
});

Nigho.directive("fichierRegister", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/register.html"
    }
});

Nigho.directive("fichierForgottenPassword", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/forgottenPassword.html"
    }
});

Nigho.directive("fichierMain", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/main.html"
    }
});

Nigho.directive("fichierSites", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/sites.html"
    }
});

Nigho.directive("fichierContacts", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/contacts.html"
    }
});

Nigho.directive("fichierReports", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/reports.html"
    }
});

Nigho.directive("fichierDetailContact", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/detailcontact.html"
    }
});

Nigho.directive("fichierAddContact", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/addcontact.html"
    }
});

Nigho.directive("fichierUpdateContact", function () {
    return {
      restrict: 'E',
      templateUrl: "templates/updatecontact.html"
    }
});


// controlleur AngularJs of the application
Nigho.controller('MyCtrl', function ($scope, $http, $ionicTabsDelegate, $ionicActionSheet, $timeout, $ionicViewSwitcher, $ionicScrollDelegate, $ionicListDelegate, $ionicSideMenuDelegate, $ionicPopup, $translate, $ionicLoading, MY_CONSTANT) {


    document.addEventListener('deviceready', function () {
        // sauvegarde du path de l'application
        // save of the path in the application
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSys) {
            window.sessionStorage.setItem('applicationPath', fileSys.root.toURL());
            checkConnection();
            $scope.$apply();
        });

        // gestion du back button
        // back button management
        document.addEventListener('backbutton', function (event) {
            var page = window.location.href.split("/").pop();
            if (page === "login") {
                navigator.app.exitApp();
            } else {
                if (page === "register" || page === "forgottenPassword") {
                    $scope.backLogin();
                }
            }
            event.preventDefault();
            event.stoppropagation();
        }, false);

        var connected = window.localStorage.getItem('userKey') || 0;
        if(connected==0){
            window.location.href = "nigho.html#/login";
        }
        else{
            $scope.userKey = JSON.parse(window.localStorage.getItem('userKey'));
            console.log($scope.userKey);
            $scope.goToMain();
        }
    }, false);

    $scope.numTab;

    /****************************
                i18n
    *******************************/

    $scope.changeLanguage = function (key) {
        $translate.use(key);
    };

    /****************************
            Authentication
    *******************************/

    $scope.userKey;  // Champs -> 'key', 'message', 'username'
    $scope.authentificationError=false;
    $scope.inputUsernameError=false;
    $scope.inputPasswordError=false;
    $scope.inputEmpty=false;
    $scope.serverError = false;

    $scope.login = function(username, password) {
        disableErrorMessages();
        if(typeof username == "undefined" && typeof password == "undefined"){
            $scope.inputEmpty=true;
        }
        else if(typeof username == "undefined"){
            $scope.inputUsernameError=true;
        }
        else if(typeof password == "undefined"){
            $scope.inputPasswordError=true;
        }
        else{
            if (username.length!==0&&password.length>=8) {
                $http({
                    method: 'POST',
                    url: 'http://report.nigho.com/auth/local',
                    data: {identifier: username, password: password}
                })
                .success(function (response) {
                    console.log(response);
                    $scope.userKey = response;
                    window.localStorage.setItem('userKey', JSON.stringify(response));
                    $scope.goToMain();
                }).error(function (err, data) {
                    var newData = parseInt(data);
                    if(newData==503){
                        $scope.serverError = true;
                    }
                    else{
                        $scope.authentificationError=true;
                    }
                });
            }
            else if(password.length<8){
                $scope.inputPasswordError=true;
            }
        } 
    };

    $scope.isLoged = function(){
        var loged = window.localStorage.getItem('userKey') || 0;
        if(loged==0){
            return false;
        }
        else{
            return true;
        }
    };

    function disableErrorMessages (){
        $scope.authentificationError=false;
        $scope.inputUsernameError=false;
        $scope.inputPasswordError=false;
        $scope.accountCreated=false;
        $scope.registerError=false;
        $scope.inputMailError=false;
        $scope.inputEmpty=false;
        $scope.authFailedError = false;
        $scope.serverError = false;
        $scope.connectionReportError = false;
        $scope.connectionSitesError = false;
    }
 

    /****************************
            Register
    *******************************/
    $scope.accountCreated=false;
    $scope.registerError=false;
    $scope.inputMailError=false;
    $scope.authFailedError = false;

    $scope.register = function(username, mail, password){
            disableErrorMessages();
            if(typeof username == "undefined" || typeof password == "undefined" || typeof mail == "undefined"){
                $scope.inputEmpty = true;
            }
            else{
                if (username.length!==0&&isValidMail(mail)&&password.length>=8) {
                    $http({
                        method: 'POST',
                        url: 'http://report.nigho.com/auth/local/register',
                        data: {username: username, email:mail, password: password}
                    })
                    .success(function (response) {
                        console.log(response);
                        $scope.userKey = response;
                        window.localStorage.setItem('userKey', JSON.stringify(response));
                        $scope.goToMain();
                    }).error(function (err, data) {
                        //err -> message d'erreur / data -> numéro de l'erreur
                        console.log(err, data);
                        if(err=="auth.failed"){
                            $scope.authFailedError = true;
                        }
                        else{
                            $scope.registerError = true;
                        }
                    });
                }
                else if(password.length<8){
                    $scope.inputPasswordError=true;
                }
                else if(!isValidMail(mail)){
                    $scope.inputMailError=true;
                }
            }
    };

    /*$scope.registerGoogle= function(){
        disableErrorMessages();
        $http({
                method: 'GET',
                url: 'http://report.nigho.com/auth/google',
            })
            .success(function (response) {
                console.log(response);
                //$scope.accountCreated=true;
                //variable montrant qu'un compte a été créé sur la page 'login'
                //window.location.href = "nigho.html#/login";
            }).error(function (err) {
                console.log(err);
                //variable montrant une erreur d'enregistrement sur la page 'register'
                //$scope.registerError=true;
            });
    };*/

    function isValidMail(mail){
        if(mail.indexOf('@')&&mail.indexOf('.')){
            return true;
        }
        else{
            return false;
        }
    }
    
    $scope.goToRegister = function () {
        disableErrorMessages();
       window.location.href = 'nigho.html#/register';
    }; 

    /****************************
                Logout
    *******************************/
    $scope.logout = function(){
        //var user = JSON.parse(window.localStorage.getItem('userKey'));
        //alert("clé : "+user.key);
        $http({
            method: 'GET',
            url: 'http://report.nigho.com/auth/logout',
        })
        .success(function (response, data) {
            console.log(response, data);
        }).error(function (err, data) {
        //err -> message d'erreur / data -> numéro de l'erreur
            console.log(err, data);
        });
    };

    $scope.showConfirmLogout = function() {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Logout',
          template: 'Are you sure you want to log out ?'
        });
        confirmPopup.then(function(res) {
          if(res) {
            $scope.logout();
            localStorage.setItem('userKey', 0);
            window.location.href = "nigho.html#/login";
          } else {
            
          }
        });
    };


    /****************************
           Forgotten Password
    *******************************/
    
    $scope.goToForgottenPassword = function () {
        disableErrorMessages();
       window.location.href = 'nigho.html#/forgottenPassword';
    };

    /****************************
            Back Button
    ****************************/
    
    $scope.back = function() {
      var page = window.location.href.split("/").pop();
      if(page === "register" || page === "forgottenPassword"){
        disableErrorMessages();
        window.location.href = 'nigho.html#/login';
      }
      else if(page === "detailcontact"){
        $scope.clearInput();
        window.location.href = "nigho.html#/contacts";
      }
      else if(page === "addcontact"){
        $scope.clearInput();
        window.location.href = "nigho.html#/contacts";
      }
      else if(page === "updatecontact"){
        $scope.clearInput();
        //$ionicListDelegate.closeOptionButtons();
        window.location.href = "nigho.html#/contacts";
      }
    };

    /***************************
            Main/Dashboard
    *****************************/

    $scope.goToMain = function (){
        checkConnection();
        $scope.numTab=0;
        window.location.href = "nigho.html#/main";
    };
    
    $scope.toggleLeft = function() {
      $ionicSideMenuDelegate.toggleLeft();
    }; 

    $scope.showLogOut = function() {
       // Show the action sheet
       var hideSheet = $ionicActionSheet.show({
         buttons: [
           { text: 'Log Out' }
         ],
         titleText: 'Options',
         cancelText: 'Cancel',
         cancel: function() {
              // add cancel code..
            },
         buttonClicked: function(index) {
                $scope.showConfirmLogout();    
            }  
       });

       // For example's sake, hide the sheet after two seconds
       $timeout(function() {
         hideSheet();
       }, 5000);

     }; 

     /***************************
               Sites
    *****************************/
    $scope.listSites;  //champ -> 'accountID', 'createdAt', 'description', 'id', 'protocolUrl', 'sessionTime', 'timeZoneOffset', 'title', 'tracks(Array)', 'updatedAt', 'url', 'visitors(Array)', 'visits(Array)'
    $scope.curSite;
    $scope.connectionSitesError = false;

    $scope.retrieveSites = function() {
        disableErrorMessages();
        $http({
                method: 'GET',
                url: 'http://report.nigho.com/api/site',
            })
            .success(function (response) {
                console.log(response);
                $scope.listSites = response;
                window.localStorage.setItem('userSites', JSON.stringify(response));
            }).error(function (err) {
                $scope.connectionSitesError = true;
                $scope.listSites = JSON.parse(window.localStorage.getItem('userSites'));
            });
    }; 

    $scope.initSites = function(){
        //permet d'initialiser la délection du dernier site par défault
    }; 

    $scope.goToSites = function(){
        checkConnection();
        $scope.numTab=1;
        window.location.href = "nigho.html#/sites";
    };

    $scope.selectSite = function(item){
        checkConnection();
        $scope.curSite = item;
        window.sessionStorage.setItem('currentSite', JSON.stringify(item));
        $scope.toggleLeft();
    };

    $scope.hasSelectedSite = function(){
        var site = window.sessionStorage.getItem('currentSite') || 0;
        if(site == 0){
            return false;
        }
        else{
            return true;
        }
    };

    $scope.initDetailSite = function() {
        $scope.curSite=JSON.parse(window.sessionStorage.getItem('currentSite'));
    };

    $scope.doRefreshSites = function() {
        $scope.retrieveSites();   //récupère tous les sites
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$apply()
    }; 


    /***************************
              Contacts
    *****************************/

    $scope.listContacts;  //champs->"visitors", "createdAt", "firstName", "lastName", "title", "updatedAt", "id", "company": {"name","createdAt","updatedAt","id"}
    $scope.curContact;
    $scope.firstName;
    $scope.lastName;
    $scope.title;
    $scope.companyName;

    $scope.goToContacts = function(){
        checkConnection();
        $scope.numTab=3;
        window.location.href = "nigho.html#/contacts";
    };

    $scope.retrieveContacts = function() {
        $http({
                method: 'GET',
                url: 'http://report.nigho.com/api/contact',
            })
            .success(function (response) {
                console.log(response);
                $scope.listContacts = response;
                window.localStorage.setItem('userContacts', JSON.stringify(response));
            }).error(function (err) {
                $scope.listContacts = JSON.parse(window.localStorage.getItem('userContacts'));
            });
    };

    $scope.doRefreshContacts = function() {
        $scope.retrieveContacts();   //récupère tous les sites
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$apply()
    };

    $scope.addContact = function(firstNameInput, lastNameInput, titleInput, companyInput){
        $http({
                method: 'POST',
                url: 'http://report.nigho.com/api/contact',
                data: {firstName:firstNameInput, lastName: lastNameInput, title:titleInput, company:{name:companyInput}}
            })
            .success(function (response) {
                console.log(response);
                $scope.clearInput();
                $scope.retrieveContacts();
                $scope.goToContacts();
            }).error(function (err) {
                console.log(err);
                $scope.goToContacts();
            });
    };

    $scope.showConfirmDeleteContact = function(item) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Delete contact',
          template: 'Are you sure you want to delete it ?'
        });
        confirmPopup.then(function(res) {
          if(res) {
            $scope.deleteContact(item);
          } else {
            
          }
        });
    };

    $scope.updateContact = function(firstNameInput, lastNameInput, titleInput, companyInput){
        var contact = JSON.parse(window.sessionStorage.getItem('currentContact'));
        $http({
                method: 'PUT',
                url: 'http://report.nigho.com/api/contact/'+contact.id,
                data: {firstName:firstNameInput, lastName: lastNameInput, title:titleInput, company:{name:companyInput}}
            })
            .success(function (response) {
                console.log(response);
                $scope.clearInput();
                $scope.retrieveContacts();
                $scope.goToContacts();
            }).error(function (err) {
                console.log(err);
                $scope.goToContacts();
            });
    };

    $scope.deleteContact = function(item){
        var idInput = item.id;
        $http({
                method: 'DELETE',
                url: 'http://report.nigho.com/api/contact',
                data: {id:idInput}
            })
            .success(function (response) {
                console.log(response);
                $scope.clearInput();
                $scope.retrieveContacts();
                $scope.goToContacts();
            }).error(function (err) {
                console.log(err);
                $scope.goToContacts();
            });
        $scope.retrieveContacts();    
    };

    $scope.goToUpdateContact = function(item){
        window.sessionStorage.setItem('currentContact', JSON.stringify(item));
        $scope.initDetailContact();
        $scope.firstName = $scope.curContact.firstName;
        $scope.lastName = $scope.curContact.lastName;
        $scope.title = $scope.curContact.title;
        $scope.company = $scope.curContact.company.name;
        $ionicListDelegate.closeOptionButtons();
        window.location.href = "nigho.html#/updatecontact";
    };

    $scope.goToAddContact = function(){
        checkConnection();
        $scope.firstName = "";
        $scope.lastName = "";
        $scope.title = "";
        $scope.company = "";
        $ionicListDelegate.closeOptionButtons();
        window.location.href = "nigho.html#/addcontact"
    }

    $scope.goToDetailContact = function(item){
        window.sessionStorage.setItem('currentContact', JSON.stringify(item));
        window.location.href = "nigho.html#/detailcontact";
    };

    $scope.initDetailContact = function() {
        $scope.curContact = JSON.parse(window.sessionStorage.getItem('currentContact'));
    };

    $scope.hasCompany = function(item) {
        if (typeof item.company == "undefined") {
            return false;
        }
        else{
            return true;
        }
    };

    $scope.hasVisitor = function(item) {
        if(item.length==0){
            return false;
        }
        else{
            return true;
        }
    };

    /***************************
              Reports
    *****************************/
    $scope.curReport;
    $scope.connectionReportError = false;

    $scope.goToReports = function(){
        checkConnection();
        $scope.numTab=2;
        window.location.href = "nigho.html#/reports";
    };

    $scope.selectReportSite = function(item){
        disableErrorMessages();
        checkConnection();
        window.sessionStorage.setItem('currentReport', JSON.stringify(item));
        $scope.curReport = item;
        $scope.toggleLeft();  
    };

    $scope.hasSelectedReport = function(){
        var report = window.sessionStorage.getItem('currentReport') || 0;
        if(report == 0){
            return false;
        }
        else{
            return true;
        }
    };

    $scope.initReport = function(){
        //permet d'initialiser la délection du dernier site par défault
    };

    /***************************
            Utilitaire
    ******************************/

    $scope.isEditable = false;
    
    $scope.formatDate = function(d) {
        var date = new Date(d);
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        d = yyyy + "-" + mm + "-" + dd;
        return d;
    };

    $scope.input = {
        search: '',
        tag: ''
    };

    // Nettoyage de l'input au niveau de la recherche
    $scope.clearInput = function () {
        $scope.input.search = "";
    };

    //Check connection
    function checkConnection() {
        var networkState = navigator.connection.type;

        var states = {};
        states[Connection.UNKNOWN]  = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI]     = 'WiFi connection';
        states[Connection.CELL_2G]  = 'Cell 2G connection';
        states[Connection.CELL_3G]  = 'Cell 3G connection';
        states[Connection.CELL_4G]  = 'Cell 4G connection';
        states[Connection.CELL]     = 'Cell generic connection';
        states[Connection.NONE]     = 'No network connection';

        console.log("Connection Mobile : "+states[networkState]);
        if(states[networkState]=="WiFi connection" || states[networkState]=="Cell 2G connection" || states[networkState]=="Cell 3G connection" || states[networkState]=="Cell 4G connection" || states[networkState]=="Cell generic connection"){
            window.sessionStorage.setItem('connected', 1);
        }
        else{
            window.sessionStorage.setItem('connected', 0);
        }
    }

                                   

});